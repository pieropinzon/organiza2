var gulp           = require('gulp'),
    del            = require('del'),
    gulpif         = require('gulp-if'),
    stylus         = require('gulp-stylus'),
    quills         = require('quills'),
    jshint         = require('gulp-jshint'),
    stylish        = require('jshint-stylish'),
    useref         = require('gulp-useref'),
    miniHTML       = require('gulp-minify-html'),
    miniCss        = require('gulp-minify-css'),
    uglify         = require('gulp-uglify'),
    imagemin       = require('gulp-imagemin'),
    pngquant       = require('imagemin-pngquant'),
    mainBowerFiles = require('gulp-main-bower-files'),
    concat         = require('concat'),
    size           = require('gulp-size');


//  CLEAN DEV FOLDERS
gulp.task('clean', function() {
  del( ['dist'] );
});

gulp.task('clean:all', ['clean'], function() {
  del( ['bower_components/', 'node_modules'] );
});
//  CLEAN DEV FOLDERS
 

//  COMPILAR ARCHIVOS STYL
 gulp.task('styles', function(){
  gulp.src('app/styles/*.styl')
    .pipe( stylus({ 'use': quills() }) )
    .pipe(gulp.dest('app/styles'));
 });
//  COMPILAR ARCHIVOS STYL


//  LINT JS
gulp.task('lint', function(){
  gulp.src('app/scripts/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter(stylish));
});
//  LINT JS


//  MINIFICA, CONTANENA HTML/CSS/JS/BOWER DEPS
gulp.task('html', ['styles'], function() {
  var assets = useref.assets();
  gulp.src('app/*.html')
    .pipe(assets)
    .pipe(gulpif('*.js',  uglify() ))
    .pipe(gulpif('*.css', miniCss({ compatibility: '*' }) ))
    .pipe(assets.restore())
    .pipe(useref())
    .pipe(gulpif('*.html', miniHTML({ conditionals: true, loose: true }) ))
    .pipe(gulp.dest('dist') );
});
//  MINIFICA, CONTANENA HTML/CSS/JS/BOWER DEPS


//  MINIFICA IMAGENES
gulp.task('images', function(){
  gulp.src('app/images/**/*')
    .pipe( imagemin({
      progressive: true,
      interlaced: true,
      svgoPlugins: [{
        removeViewBox: false,
        cleanupIDs: false
      }],
      use: [pngquant()]
    }))
    .pipe(gulp.dest('dist/images'));
});
//  MINIFICA IMAGENES




gulp.task('fonts', function() {
  gulp.src(require('gulp-main-bower-files')({
    filter: '**/*.{eot,svg,ttf,woff,woff2}'
  }).concat('app/fonts/**/*'))
    .pipe(gulp.dest('dist/fonts'));
});


//  EXTRAS
gulp.task('extras', function() {
  gulp.src([
    'app/*.*',
    '!app/*.html'
  ], { dot: true })
  .pipe(gulp.dest('dist'));
});
//  EXTRAS


// gulp.task('build', ['lint', 'html', 'images', 'fonts', 'extras'], () => {
gulp.task('build', ['lint', 'html', 'images', 'extras'], function() {
  gulp.src('dist/**/*')
    .pipe(size({
      title: 'build',
      gzip: true
    })
  );
});

gulp.task('default', ['clean'], function() {
  gulp.start('build');
});
